#include<stdio.h>
#include"phone.h"
void rm_num(PNlist* book, FILE* fp){
	printf("remove target: ");
	char target[20];
	scanf("%s", target);
	node* temp=NULL;
	node* before=NULL;
	int targetIndex=0;

	while(book->cur!=NULL){
		if(strcmp(target, book->cur->name) == 0)
			break;
		before = book->cur;
		book->cur = book->cur->next;
		targetIndex++;
	}
	if(book->cur == NULL){
		printf("Not exist\n");
		return ;
	}
	else
	{
		temp = book->cur;
		if(temp == book->head){
			book->head = NULL;
			book->tail = NULL;
			book->cur = NULL;
		}
		else if(temp == book->tail){
			book->tail = before;
			before->next = book->cur->next;
		}
		else
			before->next = book->cur->next;

		rmInFile(book, fp, targetIndex);
	}

	free(temp);
	book->num = book->num -1;
	book->cur = book->head;
	printf("\n\n%s removed!!\n", target);
}

void rmInFile(PNlist* book, FILE* fp, int targetIndex)
{
	fseek(fp, targetIndex*sizeof(node),SEEK_SET); //aim

	while(book->cur->next!=NULL){
		fwrite(book->cur->next, sizeof(node),1, fp);  //pull
		book->cur=book->cur->next;
	}

	book->cur=book->head;
	
}
