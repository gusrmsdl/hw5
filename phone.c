#include<stdio.h>
#include<malloc.h>
#include<string.h>
#include"phone.h"

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("data needed");
		return 1;
	}
	FILE *fp;
	fp = fopen(argv[1], "rb+");

	PNlist book;
	book.head = NULL;
	book.cur = NULL;
	book.tail = NULL;
	printf("Make a book\n");

	initBook(&book, fp);
	printf("init,,done\n");

	printf("Telephone number management\n");
	printf("\n");
	printf("1.Registration  2.Search All  3.Search Name  4.Remove  5.quit\n");
	printf("\n");
	int menu = 0;
	while(menu != 5)
	{
		printf("Menu Option : ");
		scanf("%d", &menu);
		switch(menu)
		{
			case 1:
				if(passwdCheck())
				{
					registerNew(&book, fp);
				}
				else
				{
					printf("Terminate Program\n");
					return 0;
				}
				break;

			case 2:
				printAll(&book);
				break;

			case 3:
				printSearch(&book);
				break;

			case 4:
				rm_num(&book, fp);
				break;
			case 5:
				printf("Bye\n");
				break;

			default:
				printf("try again\n");
				printf("1.Registration  2.Search All  3.Search Name  4.Remove  5.quit\n");
				break;

		}
	}
}
