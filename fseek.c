#include<stdio.h>
#include"student.h"

#define START_ID 100

int main(int argc, char* argv[])
{
	struct student record;
	FILE *fp;

	if(argc !=2){
		fprintf(stderr, "Direction: %s file name\n", argv[0]);
		return 1;
	}

	fp = fopen(argv[1], "wb");

	printf(" ID  NAME  SCORE\n");
	while( scanf("%d %s %d", &record.id, record.name, &record.score) == 3)
	{
		fseek(fp, (record.id - START_ID)*sizeof(record), SEEK_SET);
		fwrite(&record, sizeof(record),1, fp);
	}

	fclose(fp);
	return 0;
}
