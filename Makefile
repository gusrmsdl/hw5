all: phone

phone: phone.o passwd.o regist.o remove.o print.o
	gcc -o $@ $^

phone.o: phone.c
	gcc -c $<
passwd.o: passwd.c
	gcc -c passwd.c
register.o: regist.c
	gcc -c regist.c
remove.o: remove.c
	gcc -c remove.c
search.o: search.c
	gcc -c search.c


clean: 
	rm ./phone


