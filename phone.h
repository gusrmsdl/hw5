#include<stdio.h>

#include<malloc.h>
struct node{
	char name[20];
	char num[20];
	struct node* next ;
};
typedef struct node node;

typedef struct PNlist{
	node* head ;
	node* cur ;
	node* tail ;
	int num;
}PNlist;
void initBook(PNlist* book, FILE* fp);

int passwdCheck ();
node* createNode();
void registerNew(PNlist* book, FILE* fp);
void writeInFile(PNlist* book, FILE* fp);


void printAll(PNlist* book);
void printSearch(PNlist* book);

void rm_num(PNlist* book, FILE* fp);
void rmInFile(PNlist* book, FILE* fp, int targetIndex);
